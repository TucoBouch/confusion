$(document).ready(function () {
    $("#carousel-button").click(function () {
        var carousel = $("#mycarousel");
        var carouselButtonSpan = $("#carousel-button > span");

        carousel.carousel({ interval: 2000 });

        if (carouselButtonSpan.hasClass('fa-pause')) {
            carousel.carousel('pause');
            carouselButtonSpan.removeClass('fa-pause');
            carouselButtonSpan.addClass('fa-play');
        }
        else {
            carousel.carousel('cycle');
            carouselButtonSpan.removeClass('fa-play');
            carouselButtonSpan.addClass('fa-pause');
        }
    });

    //TASK 2 ASSIGNMENT 4
    $('#reserveTableButton').click(
        function () {
            $('#reserveModal').modal('show')
        }
    );


    //TASK 3 ASSIGNMENT 4
    $('#loginButton').click(
        function () {
            $('#loginModal').modal('show')
        }
    );


});